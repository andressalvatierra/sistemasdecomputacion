#!/usr/bin/env python
import RPi.GPIO as GPIO
import sys
import os
import time
import subprocess
sys.path.append('./MFRC522-python')
from mfrc522 import SimpleMFRC522

reader = SimpleMFRC522()
global user_log #flag que si es 1 encontro usuario en BdD, 0 si no
user_log = 0
flag_delete=0 #flag para indicar que si quiere borrar un usuario de la BdD
usuarios=[] #estructura de usuarios cargados en sistema (BdD)

def check(id,flag_delete):#funcion que recorre la BdD verificando
    if (id in usuarios):
        if (flag_delete == 1):
            usuarios.remove(id)
        return 1
    return 0

def logTime():
    return "[" + time.strftime("%Y/%m/%d-%H:%M:%S", time.localtime(time.time())) + "] "

print("SistemasDeComputacion-TPFinal - Aagaard-Izquierdo-Salvatierra")
fd = open("logfile.log",mode='w',buffering=1)
fd.write(logTime() + "Inicio de log\n")
while 1:
    print("\nSeleccione la funcion que desea ejecutar:")
    print("0 - Verificar si ya esta en sistema\n1 - Agregar usuario\n2 - Borrar usuario\n3 - Exit\n")
    operacion = int(input('Ingrese Opcion: '))
    if (operacion == 3):
        print("Fin")
        fd.write(logTime() + "Fin de log\n")
        fd.close()
        GPIO.cleanup()
        raise SystemExit
    print("Acerque su tarjeta cerca del lector..")
    id, text = reader.read()
    with open('/dev/encripter_file','w') as fout:
        subprocess.run(['echo',str(id)],stdout=fout)
    id_crypt = subprocess.check_output(["head","-c","-1","/dev/encripter_file"],encoding="utf-8")
    if(operacion==0):#usuario ya agregado intenta acceder a laboratorio
        try:
            if(check(id_crypt,0) == 1):
                os.system("./ledverde")
                print(">> Acceso permitido") #prederia el led
                fd.write(logTime() + "Acceso permitido a " + id_crypt + "\n")
            else:
                os.system("./ledrojo")
                print(">> Acceso denegado")
                fd.write(logTime() + "Acceso denegado a " + id_crypt + "\n")
        except:
            print("Something went wrong")
    elif(operacion==1): #agregando nuevo usuario
        try:
            if(check(id_crypt,0) == 1):
                print(">> Su usuario ya se encuentra en la base de datos")
                fd.write(logTime() + "Intento de agregar usuario " + id_crypt + " pero ya se encuentra en la base de datos\n")
            else:
                #deberiamos ver forma de que encripte ese id y lo almacene en la BdD
                usuarios.append(id_crypt) 
                print(">> Lista de usuarios: ", usuarios)
                print(">> Codigo de nuevo usuario: ", usuarios[len(usuarios)-1])
                print(">> Usuario agregado con exito")
                fd.write(logTime() + "Intento de agregar usuario " + id_crypt + " exitoso\n")
        except:
            print("Something went wrong")
    elif(operacion==2): #borrando usuario ya logueado
        try:
            if(check(id_crypt,1) == 0):
                print(">> Usuario no esta logeado en el sistema")
                fd.write(logTime() + "Intento de borrar usuario " + id_crypt + " pero no esta logeado en el sistema\n")
            else:
                print(">> Lista de usuarios: ", usuarios)
                print(">> Usuario eliminado con exito")
                fd.write(logTime() + "Intento de borrar usuario " + id_crypt + " exitoso\n")
        except:
            print("Something went wrong")


#122  as -o blinkrojo.o blinkrojo.asm 
#123  as -o blinkverde.o blinkverde.asm 
#124  gcc -o ledrojo blinkrojo.o ledrojo.c -lwiringPi
#125  gcc -o ledverde blinkverde.o ledverde.c -lwiringPi

