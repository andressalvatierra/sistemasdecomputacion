# Trabajo practico: Control de acceso con RFID
Sistema de control de acceso implementado con un Raspberry Pi 3

![Alt text](Wiki/SistComp.jpg?raw=true "Diagrama")

## Objetivos generales
Se pretende desarrollar un sistemas de control de acceso para un lugar que requiera restringir su entrada, como por ejemplo un laboratorio y/o evento. Los usuarios estaran registrados en una base de datos, permitiendo agregarse o quitarse miembros de la misma; las entradas/salidas tanto las validas como invalidas seran logueadas en un archivo y representadas a partir de un led verde o rojo, respectivamente. También los datos se podrán visualizar y configurar a través de una página web.

## Materiales
- Módulo RFID (RC-522)
- Raspberry pi 3 modelo B
- Servo motor o luz led para representar acceso
- Tarjeras/llaveros RFID
- Cable Ethernet
- Cables Dupont

## Integrantes
- Aagaard Martin
- Izquierdo Joffre Agustina Nahir
- Salvatierra Andrés

## Licencia
GNU GPLv2